Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: fedorainfracloud.org/coprs/besser82/libxcrypt_CI/package/libxcrypt/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/besser82/libxcrypt_CI)
License: LGPL-2.1

Files: ChangeLog
 TODO.md
Copyright: 2015-2021, Björn Esser, 2002, 2003, 2004, SuSE Linux AG, Germany, 2005, 2008, 2009, 2011 SUSE LINUX Products GmbH, Germany
License: LGPL-2.1+

Files: LICENSING
Copyright: Thorsten Kukuk, Björn Esser, Zack Weinberg; LGPL (v2.1 or later):
 Free Software Foundation, Inc.; LGPL (v2.1 or later):
 David Burren et al.; 3-clause BSD:
License: LGPL-2.1+

Files: Makefile.am
Copyright: 2002, 2007, SuSE Linux AG, Germany
License: LGPL-2.1+

Files: autogen.sh
Copyright: 2018-2021, Björn Esser <besser82@fedoraproject.org>
License: 0BSD

Files: build-aux/*
Copyright: 2008, guidod@gmx.de>, 2011, Maarten Bosmans <mkbosmans@gmail.com>
License: FSFAP

Files: debian/*
Copyright: 2018-2021, Marco d'Itri <md@linux.it>
License: LGPL-2.1+

Files: debian/patches/*
Copyright: 1999-2008, 2011-2015, Free Software Foundation, Inc.
License: FSFULLR

Files: debian/tests/libcrypt-dev
Copyright: 2021, Simon McVittie
License: LGPL-2.1

Files: doc/crypt.3
Copyright: 2000-2011, Solar Designer, 2017, 2018 Zack Weinberg, and it is
License: public-domain

Files: doc/crypt.5
 doc/crypt_gensalt.3
Copyright: 2000-2011, Solar Designer, 2017, Zack Weinberg, and it is
License: public-domain

Files: lib/alg-des-tables.c
 lib/alg-des.c
 lib/alg-des.h
 lib/crypt-des-obsolete.c
 lib/crypt-des.c
 lib/gen-des-tables.c
Copyright: 1994, David Burren
License: BSD-3-clause

Files: lib/alg-gost3411-2012-const.h
 lib/alg-gost3411-2012-core.c
 lib/alg-gost3411-2012-core.h
 lib/alg-gost3411-2012-precalc.h
 lib/alg-gost3411-2012-ref.h
Copyright: 2013, Alexey Degtyarev <alexey@renatasystems.org>.
License: BSD-2-clause

Files: lib/alg-gost3411-2012-hmac.c
 lib/alg-gost3411-2012-hmac.h
Copyright: 2018, vt@altlinux.org
 2018, Björn Esser <besser82@fedoraproject.org>
License: 0BSD

Files: lib/alg-hmac-sha1.c
 lib/alg-hmac-sha1.h
Copyright: 2017, Björn Esser <besser82@fedoraproject.org>
License: BSD-2-clause

Files: lib/alg-md4.c
 lib/alg-md4.h
 lib/alg-md5.c
 lib/alg-md5.h
Copyright: 2001, Alexander Peslyak and it is hereby released to the
License: public-domain

Files: lib/alg-sha1.h
 lib/util-gensalt-sha.c
Copyright: no-info-found
License: public-domain

Files: lib/alg-sha256.c
 lib/alg-yescrypt-opt.c
 lib/alg-yescrypt.h
Copyright: 2012-2018, 2021, Alexander Peslyak
 2005-2016, Colin Percival
License: BSD-2-clause

Files: lib/alg-sha256.h
 lib/alg-sha512.h
Copyright: 2005-2016, Colin Percival
License: BSD-2-clause

Files: lib/alg-sha512.c
Copyright: 2021, 2022, Alexander Peslyak
 2015, Allan Jude <allanjude@FreeBSD.org>
 2005, Colin Percival
License: BSD-2-clause

Files: lib/alg-yescrypt-common.c
 lib/alg-yescrypt-platform.c
Copyright: 2013-2018, 2022, Alexander Peslyak
License: 0BSD

Files: lib/crypt-bcrypt.c
Copyright: 1998-2014, Solar Designer and it is hereby released to the
License: public-domain

Files: lib/crypt-gensalt-static.c
Copyright: 2007-2017, Thorsten Kukuk
License: LGPL-2.1+

Files: lib/crypt-gost-yescrypt.c
Copyright: 2018, vt@altlinux.org
 2018, Björn Esser besser82@fedoraproject.org
License: 0BSD

Files: lib/crypt-md5.c
 lib/crypt-obsolete.h
 lib/crypt.h.in
Copyright: 1991-2020, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: lib/crypt-nthash.c
Copyright: 2017-2019, Zack Weinberg <zackw at panix.com>
 2017-2019, Björn Esser <besser82@fedoraproject.org>
 2003, Michael Bretterklieber
 1998, 1999, Whistle Communications, Inc.
 1998, 1999, Archie Cobbs <archie@freebsd.org>
License: BSD-2-clause

Files: lib/crypt-pbkdf1-sha1.c
Copyright: 2004, Juniper Networks, Inc.
License: BSD-3-clause

Files: lib/crypt-port.h
 lib/crypt.c
Copyright: 2018-2021, Björn Esser <besser82@fedoraproject.org>
 2007-2017, Thorsten Kukuk and Zack Weinberg
License: LGPL-2.1+

Files: lib/crypt-scrypt.c
Copyright: 2018, Björn Esser <besser82@fedoraproject.org>
 2013, Alexander Peslyak
License: 0BSD

Files: lib/crypt-static.c
Copyright: 2019, Björn Esser <besser82@fedoraproject.org>
 2007-2017, Thorsten Kukuk
License: LGPL-2.1+

Files: lib/crypt-sunmd5.c
Copyright: 2018, Zack Weinberg.
License: BSD-2-clause

Files: lib/crypt-yescrypt.c
Copyright: 2018, vt@altlinux.org
License: 0BSD

Files: lib/util-base64.c
 lib/util-make-failure-token.c
 lib/util-xbzero.c
 lib/util-xstrcpy.c
 lib/xcrypt.h.in
Copyright: 2018-2021, Björn Esser <besser82@fedoraproject.org>
License: 0BSD

Files: lib/util-get-random-bytes.c
Copyright: 2017, Zack Weinberg and it is hereby released to the
License: public-domain

Files: test/*
Copyright: 2015-2021, Björn Esser, 2002, 2003, 2004, SuSE Linux AG, Germany, 2005, 2008, 2009, 2011 SUSE LINUX Products GmbH, Germany
License: LGPL-2.1+

Files: test/alg-gost3411-2012-hmac.c
 test/alg-gost3411-2012.c
 test/crypt-gost-yescrypt.c
Copyright: 2018, vt@altlinux.org
 2018, Björn Esser besser82@fedoraproject.org
License: 0BSD

Files: test/alg-hmac-sha1.c
Copyright: 2017, Björn Esser <besser82@fedoraproject.org>
License: BSD-2-clause

Files: test/alg-yescrypt.c
Copyright: 2013-2018, 2022, Alexander Peslyak
License: 0BSD

Files: test/badsalt.c
 test/explicit-bzero.c
Copyright: 1991-2020, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: test/byteorder.c
Copyright: 2017, Zack Weinberg and it is hereby released to the
License: public-domain

Files: test/checksalt.c
 test/compile-strong-alias.c
 test/gensalt-nthash.c
 test/preferred-method.c
 test/short-outbuf.c
 test/special-char-salt.c
Copyright: 2018-2021, Björn Esser <besser82@fedoraproject.org>
License: LGPL-2.1+
